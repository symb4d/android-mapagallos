package com.symb4dtechnologies.mapagallos.maps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileProvider;

import java.io.ByteArrayOutputStream;

public class CustomTileProvider implements TileProvider {

    private Context ctx;

    private final int LEFT = 28281;
    private final int RIGHT = 28285;
    private final int TOP = 57353;
    private final int BOTTOM = 57357;
    private final int DEFAULT_ZOOM = 17;

    private final int DEFAULT_QUALITY = 100;

    private final int LQ_TILE_IMAGE_SIZE = 256;
    private final int HQ_TILE_IMAGE_SIZE = 512;

    private int tileImageSize;

    public CustomTileProvider(Context context) {
        this.ctx = context;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            this.tileImageSize = HQ_TILE_IMAGE_SIZE;
        } else {
            this.tileImageSize = LQ_TILE_IMAGE_SIZE;
        }

    }


    private boolean isValidCoords(int x, int y, int zoom) {
        return x >= LEFT && x <= RIGHT && y >= TOP && y <= BOTTOM && zoom == DEFAULT_ZOOM;
    }


    @Override
    public Tile getTile(int x, int y, int zoom) {
        if(isValidCoords(x,y,zoom)) {
            try {

                int content_id = ctx.getResources().getIdentifier("c_" + this.tileImageSize + "_" + x + "_" + y, "raw", ctx.getPackageName());
                Bitmap bitmap = BitmapFactory.decodeResource(ctx.getResources(), content_id);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, DEFAULT_QUALITY, stream);

                return new Tile(this.tileImageSize, this.tileImageSize, stream.toByteArray());
            } catch (Exception ex) {
                return null;
            }
        } else {
            return null;
        }
    }
}
