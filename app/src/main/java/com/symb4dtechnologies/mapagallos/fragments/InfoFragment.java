package com.symb4dtechnologies.mapagallos.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.util.Strings;
import com.symb4dtechnologies.mapagallos.R;
import com.symb4dtechnologies.mapagallos.activities.MainActivity;
import com.symb4dtechnologies.mapagallos.models.directorio.Directorio;
import com.symb4dtechnologies.mapagallos.models.directorio.DirectorioItem;
import com.symb4dtechnologies.mapagallos.services.DirectorioService;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoFragment extends Fragment {

    //private final Directorio directorio = new DirectorioService().loadDirectorio();
    private Directorio directorio;
    private DirecotrioListAdapter listAdapter;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    ExpandableListView listView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public InfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance(String param1, String param2) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        this.directorio =((MainActivity) getActivity()).getDirectorio();

        listAdapter = new DirecotrioListAdapter();


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        listView = view.findViewById(R.id.directorioListView);
        listView.setAdapter(listAdapter);
        //listView.setAdapter(listAdapter);
        Log.d("DEBUGTEST", ""+directorio);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            //Toast.makeText(context, "Info", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class DirecotrioListAdapter extends BaseExpandableListAdapter implements Filterable {

        ItemFilter itemFilter =  new ItemFilter();

        Directorio dirCopy = directorio;

        @Override
        public int getGroupCount() {
            return dirCopy.getDirectorioItems().length;
        }

        @Override
        public int getChildrenCount(int i) {
            return dirCopy.getDirectorioItems()[i].getSubareas().length;
        }

        @Override
        public Object getGroup(int i) {
            return dirCopy.getDirectorioItems()[i];
        }

        @Override
        public Object getChild(int i, int i1) {
            return dirCopy.getDirectorioItems()[i].getSubareas()[i1];
        }

        @Override
        public long getGroupId(int i) {
            return 0;
        }

        @Override
        public long getChildId(int i, int i1) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int i, boolean isSelected, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.directorio_listitem_layout, null);
            TextView areaTextView = view.findViewById(R.id.areaTextView);
            TextView edificioTextView = view.findViewById(R.id.edificioTextView);
            TextView telTextView = view.findViewById(R.id.phoneTextView);
            TextView emailTextView = view.findViewById(R.id.emailTextView);

            EditText searchDirectorioEditText = getActivity().findViewById(R.id.searchDirectorioEditText);
            searchDirectorioEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    listAdapter.getFilter().filter(charSequence);
                    for(int x = 0; x < listAdapter.getGroupCount(); x++)
                        listView.collapseGroup(x);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            ImageView imageView = view.findViewById(R.id.groupListItemImageView);
            if(isSelected){
                imageView.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            } else {
                imageView.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
            }


            if(dirCopy.getDirectorioItems()[i].getSubareas().length == 0){
                imageView.setVisibility(View.INVISIBLE);
            }

            areaTextView.setText(dirCopy.getDirectorioItems()[i].getArea());
            areaTextView.setTypeface(null, Typeface.BOLD);

            edificioTextView.setText(dirCopy.getDirectorioItems()[i].getEdificio());

            emailTextView.setText(dirCopy.getDirectorioItems()[i].getEmail());
            telTextView.setText(dirCopy.getDirectorioItems()[i].getTels().getNum());

            telTextView.setPaintFlags(telTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            final String phoneNumber = dirCopy.getDirectorioItems()[i].getTels().getNum();
            telTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + phoneNumber));
                    startActivity(intent);
                }
            });
            final String email = dirCopy.getDirectorioItems()[i].getEmail();
            emailTextView.setPaintFlags(emailTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            emailTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + email));
                    startActivity(intent);
                }
            });


            if(dirCopy.getDirectorioItems()[i].getTels().getNum() == null) {
                ((ViewGroup) telTextView.getParent()).removeView(telTextView);
                //emailTextView.
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) emailTextView.getLayoutParams();
                lp.addRule(RelativeLayout.BELOW, R.id.edificioTextView);

            }


            if(dirCopy.getDirectorioItems()[i].getEmail() == null) {
                ((ViewGroup) emailTextView.getParent()).removeView(emailTextView);
            }



            return view;
        }

        @Override
        public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.directorio_listsubitem_layout, null);
            TextView areaTextView = view.findViewById(R.id.subitemtextView);
            TextView telTextView = view.findViewById(R.id.subitemTelTextView);
            TextView extTextView = view.findViewById(R.id.subitemExtTextView);
            TextView emailTextView = view.findViewById(R.id.subitemEmailTextView);
            areaTextView.setText(dirCopy.getDirectorioItems()[i].getSubareas()[i1].getArea());
            //telTextView.setText(directorio.getDirectorioItems()[i].getSubareas()[i1].getTels().getNum());
            //emailTextView.setText(directorio.getDirectorioItems()[i].getSubareas()[i1].getEmail());
            boolean containsFaxString = containsFaxString = dirCopy.getDirectorioItems()[i].getSubareas()[i1].getTels().getNum() != null && dirCopy.getDirectorioItems()[i].getSubareas()[i1].getTels().getNum().toLowerCase().contains("fax");

            if(dirCopy.getDirectorioItems()[i].getSubareas()[i1].getTels().getNum() != null) {
                telTextView.setText((containsFaxString ? "" : "Tel: ")
                        + dirCopy.getDirectorioItems()[i].getSubareas()[i1].getTels().getNum());
                telTextView.setPaintFlags(telTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                final String phoneNumber = dirCopy.getDirectorioItems()[i].getSubareas()[i1].getTels().getNum();
                telTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + phoneNumber));
                        startActivity(intent);
                    }
                });
            } else {
                telTextView.setText("");
            }
            if(dirCopy.getDirectorioItems()[i].getSubareas()[i1].getEmail() != null)
                emailTextView.setText("Email: " + dirCopy.getDirectorioItems()[i].getSubareas()[i1].getEmail());
            else {
                ((ViewGroup) emailTextView.getParent()).removeView(emailTextView);
            }



            if(dirCopy.getDirectorioItems()[i].getSubareas()[i1].getTels().getExt() != null)
                extTextView.setText("Ext: " + dirCopy.getDirectorioItems()[i].getSubareas()[i1].getTels().getExt());
            else {
                ((ViewGroup) extTextView.getParent()).removeView(extTextView);
            }

            return view;
        }

        @Override
        public boolean isChildSelectable(int i, int i1) {
            return true;
        }

        @Override
        public Filter getFilter() {
            return this.itemFilter;
        }

        private class ItemFilter extends Filter {
            private ArrayList<DirectorioItem> nlist;
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                dirCopy = directorio;

                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                final Directorio list = dirCopy;

                int count = list.getDirectorioItems().length;
                nlist = new ArrayList<DirectorioItem>();

                DirectorioItem filterableString ;

                for (int i = 0; i < count; i++) {
                    filterableString = list.getDirectorioItems()[i];
                    if (filterableString.getArea().toLowerCase().contains(filterString)) {
                        nlist.add(filterableString);
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                dirCopy = new Directorio(nlist);
                notifyDataSetChanged();
            }

        }
    }
}
