package com.symb4dtechnologies.mapagallos.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.util.Strings;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.symb4dtechnologies.mapagallos.R;
import com.symb4dtechnologies.mapagallos.activities.MainActivity;
import com.symb4dtechnologies.mapagallos.maps.CustomTileProvider;
import com.symb4dtechnologies.mapagallos.services.MapInformationService;
import com.symb4dtechnologies.mapagallos.models.Campus;
import com.symb4dtechnologies.mapagallos.models.CampusMarker;

import java.util.ArrayList;
import java.util.List;


/**
 * Author: Eduardo Medina <jemedina.96@gmail.com>
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraMoveListener {

    private static final int TAG_CODE_PERMISSION_LOCATION = 1;

    private final double UAA_LAT = 21.912786;
    private final double UAA_LNG = -102.315500;

    private final double MAX_LAT = 21.919315;
    private final double MAX_LNG = -102.322845;
    private final double MIN_LAT = 21.908048;
    private final double MIN_LNG = -102.310353;

    private final float MIN_ZOOM = 17f;
    private final float MAX_ZOOM = 17.99f;


    private GoogleMap mGoogleMap;
    private MapView mapView;
    private View mView;
    private Campus campusInfo;
    private Button searchButton, closeSearchEditText, backToMapBtn;
    private EditText searchEditText;
    private ListView edificiosListView;
    private MarkerListItemAdapter adapter;
    private Button locateButton;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView = (MapView) mView.findViewById(R.id.mapView);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        //campusInfo = new MapInformationService(getContext()).loadCampus();
        campusInfo = ((MainActivity) getActivity()).getCampus();
        searchButton = mView.findViewById(R.id.searchBtn);
        locateButton = mView.findViewById(R.id.locationBtn);
        closeSearchEditText = mView.findViewById(R.id.closeSearchEditBox);
        backToMapBtn = mView.findViewById(R.id.backToMapBtn);
        searchEditText = mView.findViewById(R.id.searchEditText);
        edificiosListView = mView.findViewById(R.id.edificiosListView);
        edificiosListView.setTextFilterEnabled(true);

        locateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    LocationManager service = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
                    Criteria criteria = new Criteria();
                    String provider = service.getBestProvider(criteria, false);
                    @SuppressLint("MissingPermission") Location location = service.getLastKnownLocation(provider);
                    LatLng userLocation = new LatLng(location.getLatitude(),location.getLongitude());
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, MAX_ZOOM), 200, null);
                    if(userLocation.latitude < MIN_LAT ||
                            userLocation.latitude > MAX_LAT ||
                            userLocation.longitude > MIN_LNG ||
                            userLocation.longitude < MAX_LNG) {
                        Toast.makeText(getContext(), "Parece que tu ubicación actual esta fuera de la universidad.", Toast.LENGTH_SHORT).show();
                    }
                } catch(Exception ex) {
                    Toast.makeText(getContext(), "Error, no se pudo obtener tu ubicación", Toast.LENGTH_SHORT).show();
                }

            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                adapter.getFilter().filter(arg0);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }
        });


        List<String> edificiosList = new ArrayList<String>();
        for (CampusMarker marker : this.campusInfo.getMarkers()) {
            edificiosList.add(marker.getTitle());
        }
        adapter = new MarkerListItemAdapter();
        edificiosListView.setAdapter(adapter);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchButton.setVisibility(View.INVISIBLE);
                searchEditText.setVisibility(View.VISIBLE);
                closeSearchEditText.setVisibility(View.VISIBLE);
                edificiosListView.setVisibility(View.VISIBLE);
                locateButton.setVisibility(View.INVISIBLE);

            }
        });
        edificiosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                for (CampusMarker marker : campusInfo.getMarkers()) {
                    CampusMarker selectedMarker = (CampusMarker) adapter.getItem(i);
                    if (selectedMarker.equals(marker)) {
                        LatLng tmpLatLng = new LatLng(marker.getLat(), marker.getLng());

                        searchButton.setVisibility(View.VISIBLE);
                        searchEditText.setVisibility(View.INVISIBLE);
                        closeSearchEditText.setVisibility(View.INVISIBLE);
                        edificiosListView.setVisibility(View.INVISIBLE);

                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                        //Find the currently focused view, so we can grab the correct window token from it.
                        View vw = getActivity().getCurrentFocus();
                        //If no view currently has focus, create a new one, just so we can grab a window token from it
                        if (vw == null) {
                            vw = new View(getActivity());
                        }
                        imm.hideSoftInputFromWindow(vw.getWindowToken(), 0);

                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tmpLatLng, MAX_ZOOM), 200, null);

                        break;
                    }
                }
            }
        });

        closeSearchEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchButton.setVisibility(View.VISIBLE);
                searchEditText.setVisibility(View.INVISIBLE);
                closeSearchEditText.setVisibility(View.INVISIBLE);
                edificiosListView.setVisibility(View.INVISIBLE);
                locateButton.setVisibility(View.VISIBLE);
            }
        });

        backToMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CameraPosition uaa = CameraPosition.builder().target(new LatLng(UAA_LAT, UAA_LNG)).zoom(17).bearing(0).tilt(0).build();
                mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(uaa));
                backToMapBtn.setVisibility(View.INVISIBLE);
            }
        });

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_map, container, false);
        return mView;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        //TODO: Clear memory usages to optimize the behavior with the map
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        this.mGoogleMap = googleMap;
        this.mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.mGoogleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        getContext(), R.raw.map_style));
        for (CampusMarker marker : this.campusInfo.getMarkers()) {
            MarkerOptions markerOps = marker.buildGoogleMarker();
            this.mGoogleMap.addMarker(markerOps);
        }
        this.campusInfo.removeParkingItems();
        CameraPosition uaa = CameraPosition.builder().target(new LatLng(UAA_LAT, UAA_LNG)).zoom(17).bearing(0).tilt(0).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(uaa));
        TileProvider tp = new CustomTileProvider(getContext());

        googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(tp));
        mGoogleMap.setMinZoomPreference(MIN_ZOOM);
        mGoogleMap.setMaxZoomPreference(MAX_ZOOM);

        mGoogleMap.setOnMarkerClickListener(this);
        mGoogleMap.setOnCameraMoveListener(this);
        mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(getContext());
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(getContext());
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.LEFT);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(getContext());
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                if(!Strings.isEmptyOrWhitespace(marker.getSnippet()))
                    info.addView(snippet);

                return info;
            }
        });
        setupLocationPoint();
    }

    private void setupLocationPoint() {
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            mGoogleMap.setMyLocationEnabled(true);
            locateButton.setVisibility(View.VISIBLE);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    TAG_CODE_PERMISSION_LOCATION);
            try {
                mGoogleMap.setMyLocationEnabled(true);
                locateButton.setVisibility(View.VISIBLE);
            } catch(Exception ex) {
                Log.d("MAPAGALLOS","Error enabling the user location: " + ex.getMessage());
            }
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), MAX_ZOOM),200,null);
        marker.showInfoWindow();
        return true;
    }

    @Override
    public void onCameraMove() {

        if(mGoogleMap.getCameraPosition().target.latitude < MIN_LAT ||
                mGoogleMap.getCameraPosition().target.latitude > MAX_LAT ||
                mGoogleMap.getCameraPosition().target.longitude > MIN_LNG ||
                mGoogleMap.getCameraPosition().target.longitude < MAX_LNG){
            if(backToMapBtn.getVisibility() == View.INVISIBLE)
                backToMapBtn.setVisibility(View.VISIBLE);
        } else {
            if(backToMapBtn.getVisibility() == View.VISIBLE)
                backToMapBtn.setVisibility(View.INVISIBLE);
        }
    }

    @SuppressLint("MissingPermission")
    public void locationPermissionGranted() {
        try {
            mGoogleMap.setMyLocationEnabled(true);
            locateButton.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            Log.d("MAPAGALLOS","Error enabling the user location: " + ex.getMessage());
        }
    }



    class MarkerListItemAdapter extends BaseAdapter implements Filterable {
        Campus campusInfoCopy = campusInfo;
        @Override
        public int getCount() {
            return campusInfoCopy.getMarkers().length;
        }

        @Override
        public Object getItem(int i) {
            return campusInfoCopy.getMarkers()[i];
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.markers_listitem_layout, null);

            TextView title = view.findViewById(R.id.market_item_title_textView);
            title.setText(campusInfoCopy.getMarkers()[i].getTitle());

            TextView description = view.findViewById(R.id.market_item_description_textView);
            description.setText(campusInfoCopy.getMarkers()[i].getDescription());

            return view;
        }

        @Override
        public Filter getFilter() {

            Filter f = new Filter() {

                ArrayList<CampusMarker> nlist = null;
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    campusInfoCopy = campusInfo;
                    String filterString = charSequence.toString().toLowerCase();

                    FilterResults results = new FilterResults();


                    nlist = new ArrayList<>();


                    for (CampusMarker marker : campusInfoCopy.getMarkers()) {
                        if (marker.getTitle().toLowerCase().contains(filterString) ||
                                (marker.getDescription() != null && marker.getDescription().toLowerCase().contains(filterString))) {
                            nlist.add(marker);
                        }
                    }

                    results.values = nlist;
                    results.count = nlist.size();

                    return results;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    campusInfoCopy = new Campus(nlist);
                    notifyDataSetChanged();
                }
            };
            return f;
        }
    }
}
