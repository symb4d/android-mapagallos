package com.symb4dtechnologies.mapagallos.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.common.util.Strings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.symb4dtechnologies.mapagallos.App;
import com.symb4dtechnologies.mapagallos.R;

import java.util.Objects;

public class CampusMarker {
    private static final String TYPE_SP_POOL = "pool";
    public static final String TYPE_SP_LIBRARY = "library";
    public static final String TYPE_SP_PARKING = "parking";
    public static final String TYPE_SP_FOOD = "food";

    public static final String TYPE_ADMIN = "administrative";
    public static final String TYPE_SINGLE = "single";
    public static final String TYPE_HELP = "help";

    private float lat, lng;
    private String type;
    private String title;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    private final int COLOR_SINGLE = Color.argb(255, 68, 68, 244);
    private final int COLOR_ADMIN = Color.argb(255, 255, 38, 38);
    private final int COLOR_HELP = Color.argb(255, 244, 244, 68);

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private LayerDrawable makeClusterBackground() {
        ShapeDrawable mColoredCircleBackground = new ShapeDrawable(new OvalShape());
        int outlineColor = Color.WHITE;
        if(this.type.equalsIgnoreCase(CampusMarker.TYPE_SINGLE)) {
            mColoredCircleBackground.getPaint().setColor(this.COLOR_SINGLE);
            outlineColor = Color.WHITE;
        } else if(this.type.equalsIgnoreCase(CampusMarker.TYPE_ADMIN)) {
            mColoredCircleBackground.getPaint().setColor(this.COLOR_ADMIN);
            outlineColor = Color.WHITE;
        } else if(this.type.equalsIgnoreCase(CampusMarker.TYPE_HELP)) {
            mColoredCircleBackground.getPaint().setColor(this.COLOR_HELP);
            outlineColor = Color.BLACK;
        }
        ShapeDrawable outline = new ShapeDrawable(new OvalShape());
        outline.getPaint().setColor(outlineColor); // Transparent white.
        LayerDrawable background = new LayerDrawable(new Drawable[]{outline, mColoredCircleBackground});
        int strokeWidth = 2;
        background.setLayerInset(1, strokeWidth, strokeWidth, strokeWidth, strokeWidth);
        return background;
    }

    public MarkerOptions buildGoogleMarker() {
        BitmapDescriptor bmD = null;
        if(!isSpecialMarker()) {
            IconGenerator icg = new IconGenerator(App.getAppContext());
            icg.setBackground(makeClusterBackground());
            if (this.type.equalsIgnoreCase(CampusMarker.TYPE_SINGLE)) {
                icg.setTextAppearance(R.style.whiteText);
            } else if (this.type.equalsIgnoreCase(CampusMarker.TYPE_ADMIN)) {
                icg.setTextAppearance(R.style.whiteText);
            } else if (this.type.equalsIgnoreCase(CampusMarker.TYPE_HELP)) {
                icg.setTextAppearance(R.style.blackText);
            }
            bmD = BitmapDescriptorFactory.fromBitmap(icg.makeIcon(this.title));
        } else {
            bmD = getSpecialIcon();
        }
        String title = this.title;
        if(this.title.matches("\\d+(?:\\.\\d+)?")){
            title = "Edificio " + this.title;
        }


        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions = markerOptions.position(new LatLng(this.lat, this.lng))
                .title(title).icon(bmD).snippet(this.description);
        return markerOptions;
    }

    private BitmapDescriptor getSpecialIcon() {
        if(this.type.toLowerCase().equalsIgnoreCase(CampusMarker.TYPE_SP_LIBRARY)) {
            return bitmapDescriptorFromVector(R.drawable.marker_ic_book_solid);
        } if(this.type.toLowerCase().equalsIgnoreCase(CampusMarker.TYPE_SP_POOL)) {
            return bitmapDescriptorFromVector(R.drawable.marker_ic_swimmer_solid);
        } if(this.type.toLowerCase().equalsIgnoreCase(CampusMarker.TYPE_SP_PARKING)) {
            return bitmapDescriptorFromVector(R.drawable.marker_ic_parking_solid_16dp);
        } if(this.type.toLowerCase().equalsIgnoreCase(CampusMarker.TYPE_SP_FOOD)) {
            return bitmapDescriptorFromVector(R.drawable.marker_ic_utensils_solid);
        } else {
            return bitmapDescriptorFromVector(R.drawable.marker_ic_place_black_24dp);
        }
    }

    private boolean isSpecialMarker() {
        return !this.type.equalsIgnoreCase(CampusMarker.TYPE_SINGLE) &&
            !this.type.equalsIgnoreCase(CampusMarker.TYPE_ADMIN) &&
            !this.type.equalsIgnoreCase(CampusMarker.TYPE_HELP);
    }

    private BitmapDescriptor bitmapDescriptorFromVector(@DrawableRes int vectorDrawableResourceId) {
        Context context = App.getAppContext();
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public String toString() {
        return "CampusMarker{" +
                "lat=" + lat +
                ", lng=" + lng +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CampusMarker)) return false;
        CampusMarker that = (CampusMarker) o;
        return Float.compare(that.lat, lat) == 0 &&
                Float.compare(that.lng, lng) == 0;
    }
}
