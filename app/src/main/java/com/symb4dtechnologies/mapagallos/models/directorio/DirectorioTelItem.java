package com.symb4dtechnologies.mapagallos.models.directorio;

public class DirectorioTelItem {
    private String num;

    private String ext;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    @Override
    public String toString() {
        return "DirectorioTelItem{" +
                "num='" + num + '\'' +
                ", ext='" + ext + '\'' +
                '}';
    }
}
