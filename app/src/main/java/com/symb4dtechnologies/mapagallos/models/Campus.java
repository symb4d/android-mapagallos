package com.symb4dtechnologies.mapagallos.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Campus {
    private CampusMarker [] markers;

    public Campus(ArrayList<CampusMarker> nlist) {
        this.markers = nlist.toArray(new CampusMarker[nlist.size()]);
    }

    public Campus(){}

    public CampusMarker[] getMarkers() {
        return markers;
    }

    public void setMarkers(CampusMarker[] markers) {
        this.markers = markers;
    }

    @Override
    public String toString() {
        return "Campus{" +
                "markers=" + Arrays.toString(markers) +
                '}';
    }

    public void removeParkingItems() {
        List result = new LinkedList();

        for(CampusMarker item : markers)
            if(!item.getType().equalsIgnoreCase("parking"))
                result.add(item);

        this.markers = (CampusMarker[]) result.toArray(new CampusMarker[result.size()]);
    }
}
