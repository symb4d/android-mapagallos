package com.symb4dtechnologies.mapagallos.models.directorio;

import java.util.Arrays;

public class DirectorioItem {
    private String typo;
    private String area;
    private String edificio;
    private DirectorioTelItem tels;
    private String email;
    private DirectorioItem [] subareas;

    @Override
    public String toString() {
        return "DirectorioItem{" +
                "typo='" + typo + '\'' +
                ", area='" + area + '\'' +
                ", edificio='" + edificio + '\'' +
                ", email='" + email + '\'' +
                ", tels=" + tels +
                ", subareas=" + Arrays.toString(subareas) +
                '}';
    }

    public String getTypo() {
        return typo;
    }

    public void setTypo(String typo) {
        this.typo = typo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DirectorioTelItem getTels() {
        return tels;
    }

    public void setTels(DirectorioTelItem tels) {
        this.tels = tels;
    }

    public DirectorioItem[] getSubareas() {
        return subareas;
    }

    public void setSubareas(DirectorioItem[] subareas) {
        this.subareas = subareas;
    }
}
