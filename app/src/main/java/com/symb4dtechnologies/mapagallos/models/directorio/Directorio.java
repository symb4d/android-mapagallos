package com.symb4dtechnologies.mapagallos.models.directorio;

import java.util.ArrayList;
import java.util.Arrays;

public class Directorio {
    DirectorioItem [] directorioItems;

    public DirectorioItem[] getDirectorioItems() {
        return directorioItems;
    }

    public Directorio() {}

    public Directorio (ArrayList<DirectorioItem> items) {
        directorioItems = items.toArray(new DirectorioItem[items.size()]);
    }
    @Override
    public String toString() {
        return "Directorio{" +
                "directorioItems=" + Arrays.toString(directorioItems) +
                '}';
    }

    public void setDirectorioItems(DirectorioItem[] directorioItems) {
        this.directorioItems = directorioItems;
    }
}
