package com.symb4dtechnologies.mapagallos.activities;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.AdRequest;

import android.util.TypedValue;
import android.view.MenuItem;
import android.widget.Toast;

import com.symb4dtechnologies.mapagallos.App;
import com.symb4dtechnologies.mapagallos.R;
import com.symb4dtechnologies.mapagallos.fragments.InfoFragment;
import com.symb4dtechnologies.mapagallos.fragments.MapFragment;
import com.symb4dtechnologies.mapagallos.models.Campus;
import com.symb4dtechnologies.mapagallos.models.directorio.Directorio;
import com.symb4dtechnologies.mapagallos.services.DirectorioService;
import com.symb4dtechnologies.mapagallos.services.MapInformationService;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FragmentManager fragmentManager;
    private AdView mAdView;
    private MapFragment mapFragment;
    private Directorio directorio;
    private Campus campus;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            //super.onBackPressed();
            this.finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Presiona ATRAS de nuevo para salir", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public Directorio getDirectorio(){ return this.directorio; };
    public Campus getCampus(){ return this.campus; };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        String directorioJsonResponse = intent.getStringExtra("directorioJsonResponse");
        String markersJsonResponse = intent.getStringExtra("markersJsonResponse");
        this.directorio = DirectorioService.loadDirectorio(directorioJsonResponse);
        this.campus = MapInformationService.loadCampus(markersJsonResponse);

        MobileAds.initialize(this, "ca-app-pub-5679206260651403~6400557280");

        mapFragment = new MapFragment();
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("D5004FAB486A34197E364751BF7FBCBB").addTestDevice("2F25BCDA543810F50B0641CDCCE73E7B").build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new  com.google.android.gms.ads.AdListener(){
         @Override
            public void onAdFailedToLoad(int errorCode) {
             android.util.Log.d("DEBUGTEST","ERROR: "+ errorCode);
            }

            @Override
            public void onAdLoaded() {
                Resources r = getApplicationContext().getResources();
                int px = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 50,
                        r.getDisplayMetrics()
                );
                ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) MainActivity.this.findViewById(R.id.fragment_main_container).getLayoutParams();
                lp.setMargins(0,px, 0,0);
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.fragment_main_container, mapFragment).commit();


        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }
/*
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }*/


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        //Toast.makeText(this, "test permission xd", Toast.LENGTH_SHORT).show();
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && mapFragment != null)
            mapFragment.locationPermissionGranted();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        String fragmentSelected = MapFragment.class.toString();
        MapFragment mapFragment = new MapFragment();
        InfoFragment infoFragment = new InfoFragment();
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if(!fragmentSelected.equals(MapFragment.class.toString())) {
                        fragmentSelected = MapFragment.class.toString();
                        fragmentTransaction.replace(R.id.fragment_main_container, mapFragment).commit();
                        return true;
                    }
                    return false;
                case R.id.navigation_notifications:
                    if(!fragmentSelected.equals(InfoFragment.class.toString())) {
                        fragmentSelected = InfoFragment.class.toString();
                        fragmentTransaction.replace(R.id.fragment_main_container, infoFragment).commit();
                        return true;
                    }
                    return false;
            }
            return false;
        }
    };

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
        } else if(id == R.id.nav_rate) {
            Uri uri = Uri.parse("market://details?id=" + App.getAppContext().getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + App.getAppContext().getPackageName())));
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
