package com.symb4dtechnologies.mapagallos.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.symb4dtechnologies.mapagallos.R;
import com.symb4dtechnologies.mapagallos.models.Campus;
import com.symb4dtechnologies.mapagallos.models.directorio.Directorio;
import com.symb4dtechnologies.mapagallos.services.DirectorioService;
import com.symb4dtechnologies.mapagallos.services.MapInformationService;

public class LoadingActivity extends AppCompatActivity {
    String markersJsonResponse, directorioJsonResponse;

    final String markersEndpoint = "https://mapagallosapi.herokuapp.com/markers",
            directorioEndpoint = "https://mapagallosapi.herokuapp.com/directorio";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        final TextView mTextView = findViewById(R.id.text);

        RequestQueue queue = Volley.newRequestQueue(this);

        mTextView.setText("Cargando marcadores....");
        StringRequest markersRequest = new StringRequest(Request.Method.GET, markersEndpoint,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        markersJsonResponse = response;
                        mTextView.setText("Cargando diectorio....");
                        // Display the first 500 characters of the response string.




                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTextView.setText("Se produjo un error al cargar los marcadores... :(");
            }


        });

        StringRequest directorioRequest = new StringRequest(Request.Method.GET, directorioEndpoint,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        directorioJsonResponse = response;
                        mTextView.setText("Carga lista!");
                        //Directorio directorio = DirectorioService.loadDirectorio(directorioJsonResponse);
                        //Campus campus = MapInformationService.loadCampus(markersJsonResponse);
                        Intent i = new Intent(LoadingActivity.this, MainActivity.class);
                        i.putExtra("directorioJsonResponse",directorioJsonResponse);
                        i.putExtra("markersJsonResponse", markersJsonResponse);
                        startActivity(i);



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTextView.setText("Se produjo un error al cargar los marcadores... :(");
            }
        });

// Add the request to the RequestQueue.
        queue.add(markersRequest);
        queue.add(directorioRequest);
    }
}
