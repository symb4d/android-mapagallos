package com.symb4dtechnologies.mapagallos.services;

import android.content.Context;

import com.google.gson.Gson;
import com.symb4dtechnologies.mapagallos.R;
import com.symb4dtechnologies.mapagallos.models.Campus;

import java.io.IOException;
import java.io.InputStream;

public class MapInformationService {
    private Context ctx;

    public MapInformationService(Context ctx) {
        this.ctx = ctx;
    }

    private String getLocalJsonString() {
        try {
            InputStream is = ctx.getResources().openRawResource(R.raw.uaa_markers);
            byte[] buffer = new byte[0];

            buffer = new byte[is.available()];

            while (is.read(buffer) != -1);

            return new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*public Campus loadCampus() {
        return new Gson().fromJson(getLocalJsonString(), Campus.class);
    }*/

    public static final Campus loadCampus(String markersJson) {
        return new Gson().fromJson(markersJson, Campus.class);
    }

}
