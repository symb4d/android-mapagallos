package com.symb4dtechnologies.mapagallos.services;

import com.google.gson.Gson;
import com.symb4dtechnologies.mapagallos.App;
import com.symb4dtechnologies.mapagallos.R;
import com.symb4dtechnologies.mapagallos.models.directorio.Directorio;

import java.io.IOException;
import java.io.InputStream;

public class DirectorioService {

    private String getLocalJsonString() {
        try {
            InputStream is = App.getAppContext().getResources().openRawResource(R.raw.directorio);
            byte[] buffer = new byte[0];

            buffer = new byte[is.available()];

            while (is.read(buffer) != -1);

            return new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*public Directorio loadDirectorio() {
        return new Gson().fromJson(getLocalJsonString(), Directorio.class);
    }*/

    public static final Directorio loadDirectorio(String directorioJson) {
        return new Gson().fromJson(directorioJson, Directorio.class);
    }


}
